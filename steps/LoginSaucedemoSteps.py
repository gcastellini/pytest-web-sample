from pages import LoginSaucedemoPage as page
from core.steps.BaseSteps import BaseStep
from core.assertion.Assertion import Assertion


class LoginSauceDemoSteps(BaseStep):

 def setUsername(self,text):
    elem = page.getUsernameInput()
    elem.setText(text)
    Assertion.assertTrue('Username is not displayed',page.getUsernameInput().isDisplayed())
    return self

 def setPassword(self,text):
    elem = page.getPasswordInput()
    elem.setText(text)
    Assertion.assertTrue('Password is not displayed',page.getUsernameInput().isDisplayed())
    return self

 def clickButton(self):
    elem = page.getLoginButton()
    elem.click()
    return self

 def showCart(self):
    elem = page.getCartButton()
    Assertion.assertTrue('No se muestra el home',elem.isDisplayed())
    return self

 def showError(self,error):
     elem = page.getErrorMessage()
     Assertion.assertEquals('El mensaje no es equivalente',error,elem.getText())