from pytest_bdd import then, when, scenarios
from steps.LoginSaucedemoSteps import LoginSauceDemoSteps as sauce

scenarios('../features/LoginSauce.feature')

@when('el user ingresa el <username>')
def username(username):
    sauce().setUsername(username)

@when('el user ingresa la <password>')
def password(password):
    sauce().setPassword(password)

@when('el user clickea en login')
def clickLogin():
    sauce().clickButton()

@then('se muestra la Home Page')
def showHomePage():
    sauce().showCart()

@then('se muestra el error <error>')
def showError(error):
    sauce().showError(error)

def teardown():
    sauce().closeBrowser()