Feature: Login


  Scenario Outline: User logs in Sauce Demo page successfully
    When el user ingresa el <username>
    And el user ingresa la <password>
    And el user clickea en login
    Then se muestra la Home Page

    Examples:
      | username      | password     |
      | standard_user | secret_sauce |

  @login
  Scenario Outline: User cannot log in Sauce Demo page - wrong password
    When el user ingresa el <username>
    And el user ingresa la <password>
    And el user clickea en login
    Then se muestra el error <error>

    Examples:
      | username      | password       | error                                                                     |
      | standard_user | secret_saucess | Epic sadface: Username and password do not match any user in this service |